#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

class DataManager:

    # Constants
    CONFIG_JSON = "config/config.json"
    MESSAGES_FILE = "config/messages.json"
    MOD_ROLES_FILE = "config/mod_roles.json"
    FAQ_FILE = "config/faq.json"

    ADMIN_HELP_FILE = "files/admin_help.txt"
    ADMIN_MUSICHELP_FILE = "files/admin_musichelp.txt"
    USER_HELP_FILE = "files/user_help.txt"
    USER_MUSICHELP_FILE = "files/user_musichelp.txt"
    BOT_ANSWERS_FILE = "files/bot_answers.txt"

    def __init__ (self):
        self.config = []
        self.messages = []
        self.mod_roles = []
        self.faq = []
        self.admin_help = ""
        self.admin_musichelp = ""
        self.user_help = ""
        self.user_musichelp = ""
        self.bot_answers = []


    # Get Data Methods
    def get_config(self):
        if not self.config:
            self.load_config()
        return self.config

    def get_messages(self):
        if not self.messages:
            self.load_messages()
        return self.messages

    def get_mod_roles(self):
        if not self.mod_roles:
            self.load_mod_roles()
        return self.mod_roles

    def get_faq(self):
        if not self.faq:
            self.load_faq()
        return self.faq

    def get_admin_help_file(self):
        if not self.admin_help:
            self.load_admin_help_file()
        return self.admin_help

    def get_admin_musichelp_file(self):
        if not self.admin_musichelp:
            self.load_admin_musichelp_file()
        return self.admin_musichelp

    def get_user_help_file(self):
        if not self.user_help:
            self.load_user_help_file()
        return self.user_help

    def get_user_musichelp_file(self):
        if not self.user_musichelp:
            self.load_user_musichelp_file()
        return self.user_musichelp

    def get_bot_answers_file(self):
        if not self.bot_answers:
            self.load_bot_answers_file()
        return self.bot_answers


    # Load Methods
    def load_config(self):
        with open(self.CONFIG_JSON) as data_file:
            self.config = json.load(data_file)

    def load_messages(self):
        with open(self.MESSAGES_FILE) as data_file:
            self.messages = json.load(data_file)

    def load_mod_roles(self):
        with open(self.MOD_ROLES_FILE) as data_file:
            self.mod_roles = json.load(data_file)

    def load_faq(self):
        with open(self.FAQ_FILE) as data_file:
            self.faq = json.load(data_file)

    def load_admin_help_file(self):
        with open(self.ADMIN_HELP_FILE) as data_file:
            self.admin_help = data_file.read()

    def load_admin_musichelp_file(self):
        with open(self.ADMIN_MUSICHELP_FILE) as data_file:
            self.admin_musichelp = data_file.read()

    def load_user_help_file(self):
        with open(self.USER_HELP_FILE) as data_file:
            self.user_help = data_file.read()

    def load_user_musichelp_file(self):
        with open(self.USER_MUSICHELP_FILE) as data_file:
            self.user_musichelp = data_file.read()

    def load_bot_answers_file(self):
        with open(self.BOT_ANSWERS_FILE) as data_file:
            self.bot_answers = data_file.readlines()
