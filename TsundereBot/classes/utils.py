#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time
import threading
import asyncio
import traceback

class Utils:

    def get_channel_from_name(self, server, channel_name):
        for channel in list(server.channels):
            if channel.name == channel_name:
                return channel
        return None

    def get_member_from_id(self, client, user_id):
        for server in list(client.servers):
            for member in list(server.members):
                if member.id == user_id:
                    return member
        return None

    def get_member_from_user(self, client, user):
        for server in list(client.servers):
            for member in list(server.members):
                if member.id == user.id:
                    return member
        return None

    def get_random_from_list(self, list):
        return random.choice(list)

    def get_rol_from_message(self, msj, roles):
        for rol in roles:
            if rol.name.lower() in msj.lower():
                return rol
        return None

    def search_role(self, busq, list):
        for rol in list:
            if rol.name.lower() == busq.lower():
                return rol
        return None

    def search_roles_no_colours(self, member):
        elim = list()
        for rol in member.roles:
            if 'col_' not in rol.name:
                elim.append(rol)
        return elim

    def check_user_is_mod_or_admin(self, member, mod_roles):
        for rol in member.roles:
            for mod_role in mod_roles:
                if rol.name.lower() == mod_role.lower():
                    return True
        return False

    def check_user_manage_messages(self, member):
        for rol in member.roles:
            if rol.permissions.manage_messages:
                return True
        return False

    def check_user_manage_roles(self, member):
        for rol in member.roles:
            if rol.permissions.manage_roles:
                return True
        return False

    def check_valid_channel(self, channel, invalid_list):
        for channel_name in invalid_list:
            if channel.name == channel_name:
                return False
        return True

    async def delete_channel_messages(self, channel, number, client):
        async for msj in client.logs_from(channel, limit=number):
            await client.delete_message(msj)

    async def delete_channel_messages_from_members(self, channel, members, number, client):
        async for msj in client.logs_from(channel, limit=number):
            for member in members:
                if msj.author == member:
                    await client.delete_message(msj)
                    break

    async def set_role_time_thread(self, role, member, total_time, client, cond):
        actual_roles = member.roles
        await client.replace_roles(member, role)
        try:
            await asyncio.wait_for(cond,total_time)
        except asyncio.TimeoutError:
            traceback.print_exc()
        await client.replace_roles(member, *(actual_roles))
